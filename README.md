# 2DCarAI
Pytorch NEAT(ish) algorithm for 2d car to learn to drive. Included game and level editor.
## Custom Map
To make your own map simply delete "map.csv" or to make a different map on every run,
delete the "map.csv" passed to the CarGame constructor
## Warnings
### Installation
When running this, make sure to use with conda environment and run command:
```
conda install --file "requirements.txt"
```
RUN ONLY ON DEVICE WITH NVIDIA GPU. This was a weekend project and was written to run on cuda enabled devices only.
### Map
If you want to use the default map make sure you have a display of 1920x1080 or larger otherwise map may not fit on screen