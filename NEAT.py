import torch
import torch.nn as nn
import torch.functional as F
import torch.optim as optim
from copy import copy
import numpy as np
from time import sleep, time_ns
import pygame as pg

GREEN = (0,255,0)

class Brain(nn.Module):
    def __init__(self):
        super(Brain, self).__init__()
        torch.device('cuda')
        self.layers = nn.ModuleList([
            nn.Linear(8,10),
            nn.Linear(10,10),
            nn.Linear(10,2)
        ])
        self.randomize()

        self.topology = [8,10,2]

        self.cuda()
        self.eval()


    def forward(self, x):
        for layer in self.layers:
            x = torch.tanh(layer(x))
        return x

    def randomize(self):
        for layer in self.layers:
            layer.weight.data = torch.randn(layer.weight.size()).cuda()

    def get_pred(self, x):
        x = self.forward(torch.Tensor(x).cuda())
        x = x.cpu()
        x = x.detach().numpy()
        return x
    
    def get_all_weights(self):
        ttl = []
        for layer in self.layers:
            l = copy(layer.weight)
            l = torch.flatten(l)
            l = l.cpu()
            l = l.detach().numpy()
            ttl = np.concatenate((ttl,l))
        return ttl

    def mutate(self):
        for layer in self.layers:
            prev = layer.weight.data
            layer.weight.data = prev + torch.randn(layer.weight.size()).cuda()/5.0

    def draw_graph(self, win=None, WIDTH=600, HEIGHT=800, del_t=0, node_color=(255,255,255), pos_color = (255,0,0), neg_color = (0,0,255)):
        pg.init()
        if win==None:
            HEIGHT = 600
            WIDTH = 800
            win = pg.display.set_mode((WIDTH, HEIGHT))
        
        win.fill((255,255,255))
        nodes = []
        wnum = len(self.layers)
        dw = int(WIDTH/wnum)
        mid = int(HEIGHT/2)
        x = dw/2
        min_dh = HEIGHT
        for l in self.topology:
            layer = []
            dh = int(HEIGHT/(l))
            if dh<min_dh:
                min_dh = dh
            y = mid - dh*(l/2) + dh/2
            for i in range(l):
                layer.append((x,y))
                #pg.draw.circle(win, (255,0,0), (x,y), dw/8)
                y+=dh
            nodes.append(layer)
            x+=dw
        weights = self.get_all_weights()
        wi=0
        for i,l in zip(range(len(nodes)),nodes):
            if(i==(len(nodes)-1)):
                break
            for n in l:
                for nl in nodes[i+1]:
                    color = (0,0,0)
                    if(weights[wi]<0):
                        color = neg_color
                    else:
                        color = pos_color
                    pg.draw.line(win, color, n, nl, round(abs(weights[wi])*5))
                    pg.display.flip()
                    wi+=1
                pg.draw.circle(win, node_color, n, min_dh/3)
        for n in nodes[len(nodes)-1]:
            pg.draw.circle(win, node_color, n, min_dh/3)

        pg.display.flip()
        if del_t>0:
            sleep(del_t)
        else:
            wait = True
            while wait:
                for event in pg.event.get():
                    if event.type == pg.KEYDOWN and event.key == pg.K_RETURN:
                        wait = False
                        break
                    elif event.type == pg.QUIT:
                        wait = False
                        break
        #pg.quit()

    def save_brain(self, location="./", name="hero"):
        torch.save(self.state_dict(), location+name+".pt")

    def load_brain(self,location="./",name="hero"):
        self.load_state_dict(torch.load(location+name+".pt"))


def straighten():
    b = Brain()
    b.train()
    crit = nn.MSELoss()
    opt = optim.SGD(b.parameters(), lr = 0.03)
    for i in range(5000):
        out = b.forward(torch.randn(8).cuda())
        opt.zero_grad()
        targ = torch.Tensor([1.0,0.0]).cuda()
        loss = crit(out,targ)
        loss.backward()
        opt.step()
    return b

if __name__=="__main__":
    #b = straighten()
    b = Brain()
    b.train()
    b.draw_graph(node_color=GREEN)
    print(b.forward(torch.Tensor([1, 0.7, 0.5, 0.7, 1.0, 0.75, 0.55, 0.79]).cuda()))