import torch
import torch.nn as nn
import torch.functional as F
import torch.optim as optim
from copy import copy
import numpy as np
import pygame as pg
from time import sleep, time_ns
from random import random, randint

class Brain(nn.Module):
    def __init__(self):
        super(Brain, self).__init__()

        self.layers = nn.ModuleList([
            nn.Linear(8,10),
            nn.Linear(10,10),
            nn.Linear(10,2)
        ])

        self.topology = [8,10,10,2]

        torch.device('cuda')

    def forward(self, x, cuda=True):
        for layer in self.layers:
            x = torch.tanh(layer(x))
        return x

    def randomize(self):
        for layer in self.layers:
            layer.weight.data = torch.randn(layer.weight.size())

    def mutate(self):
        for layer in self.layers:
            s = layer.weight.size()
            for j in range(s[0]):
                for i in range(s[1]):
                    if randint(0,10)<5:
                        layer.weight[j,i] = (random()-0.5)/10.0

    def get_pred(self, x):
        x = self.forward(torch.Tensor(x))
        x = x.detach().numpy()
        print(x)
        return x[0]
    
    def get_all_weights(self):
        ttl = []
        for layer in self.layers:
            l = copy(layer.weight)
            l = torch.flatten(l)
            l = l.detach().numpy()
            ttl = np.concatenate((ttl,l))
        return ttl

    def draw_graph(self, win=None, WIDTH=600, HEIGHT=800, node_color=(255,255,255), pos_color = (255,0,0), neg_color = (0,0,255)):
        pg.init()
        if win==None:
            HEIGHT = 600
            WIDTH = 800
            win = pg.display.set_mode((WIDTH, HEIGHT))
        
        win.fill((255,255,255))
        nodes = []
        wnum = len(self.layers)
        dw = int(WIDTH/(wnum+1))
        mid = int(HEIGHT/2)
        x = dw/2
        for l in self.topology:
            layer = []
            dh = int(HEIGHT/(l))
            y = mid - dh*(l/2) + dh/2
            for i in range(l):
                layer.append((x,y))
                #pg.draw.circle(win, (255,0,0), (x,y), dw/8)
                y+=dh
            nodes.append(layer)
            x+=dw
        weights = self.get_all_weights()
        wi=0
        for i,l in zip(range(len(nodes)),nodes):
            if(i==(len(nodes)-1)):
                break
            for n in l:
                for nl in nodes[i+1]:
                    color = (0,0,0)
                    if(weights[wi]<0):
                        color = (0,0,255)
                    else:
                        color = (255,0,0)
                    pg.draw.line(win, color, n, nl, round(abs(weights[wi])*10))
                    pg.display.flip()
                    wi+=1
                pg.draw.circle(win, (0,255,0), n, dw/8)
        for n in nodes[len(nodes)-1]:
            pg.draw.circle(win, (0,255,0), n, dw/8)

        pg.display.flip()
        wait = True
        while wait:
            for event in pg.event.get():
                if event.type == pg.KEYDOWN and event.key == pg.K_RETURN:
                    wait = False
                    break
                elif event.type == pg.QUIT:
                    wait = False
                    break
        #pg.quit()

if __name__=="__main__":
    pg.init()
    WIDTH = 1500
    HEIGHT = 900
    win = pg.display.set_mode((WIDTH, HEIGHT))
    win.fill((255,255,255))
    b = Brain()
    b.train()
    crit = nn.MSELoss()
    opt = optim.SGD(b.parameters(), lr = 0.03)
    b.draw_graph(win, WIDTH, HEIGHT)
    for i in range(1000):
        out = b.forward(torch.Tensor([
            1.0,
            0.7,
            0.3,
            0.7,
            1.0,
            0.7,
            0.3,
            0.7
        ])).cuda()
        print(out*100)
        opt.zero_grad()
        targ = torch.Tensor([0,1.0]).cuda()
        loss = crit(out,targ)
        loss.backward()
        opt.step()
        if(i%100==0):
            b.draw_graph(win, WIDTH, HEIGHT)
    pg.quit()