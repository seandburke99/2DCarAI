import pygame as pg
import torch
from torch.serialization import save
from NEAT import Brain, straighten
from copy import copy, deepcopy
import csv
from time import time, time_ns, sleep
import numpy as np

DARKGRAY = (50,50,50)
GRAY = (150,150,150)
RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
WHITE = (255,255,255)
ORANGE = (255,165,0)
BLACK = (0,0,0)

def ccw(A,B,C):
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)

# Return true if line segments AB and CD intersect
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def inter_p(A,B,C,D):
    d = (D.y - C.y) * (B.x - A.x) - (D.x - C.x) * (B.y - A.y)
    if d:
        uA = ((D.x - C.x)*(A.y - C.y) - (D.y - C.y)*(A.x - C.x)) / d
        uB = ((B.x - A.x)*(A.y - C.y) - (B.y - A.y)*(A.x - C.x)) / d
    else:
        return
    if not(0 <= uA <= 1 and 0 <= uB <= 1):
        return
    x = A.x + uA * (B.x - A.x)
    y = A.y + uA * (B.y - A.y)
 
    return (x, y)


OG_SAFTEY = (
            pg.Vector2(-10,-5),
            pg.Vector2(10,-5),
            pg.Vector2(10,5),
            pg.Vector2(-10,5),
)

class Car():
    def __init__(self, x, y, id):
        self.next_move = [0,0]
        self.speed = 10
        self.turnspeed = 5
        self.id = -1
        self.goal = False
        self.rect = pg.Rect(x-10,y-5,20,10)
        self.dir = pg.Vector2(1,0)
        self.brain = Brain()
        self.id = id
        self.score = 0
        self.time = 0
        self.gas = 0
        self.col = False
        self.slines = [
            pg.Vector2(-10,-5),
            pg.Vector2(10,-5),
            pg.Vector2(10,5),
            pg.Vector2(-10,5),
        ]
        for l in self.slines:
            l += self.rect.center
        self.driver = self.rect.center + self.dir*10
        self.eyes = []
        self.vision = []
        for i in range(8):
            self.eyes.append((
                pg.Vector2(self.rect.center),
                self.dir.rotate(i*(360/8))*75 + self.rect.center
            ))
            self.vision.append(pg.Vector2(self.eyes[i][1]-self.eyes[i][0]).magnitude())
        #pg.draw.rect(self.surf, BLUE, pg.Rect(0,0, 20, 10))

    def update(self, gas, turn):
        if self.col:
            self.gas = 0
            return
        if gas:
            self.gas = self.speed*gas
            if turn:
                self.dir.rotate_ip(turn*self.turnspeed)
                self.dir.normalize_ip()
        else:
            self.gas = 0
        self.rect.move_ip(self.dir*self.gas)
        self.slines[0] = OG_SAFTEY[0].rotate(-self.dir.angle_to((1,0))) + self.rect.center
        self.slines[1] = OG_SAFTEY[1].rotate(-self.dir.angle_to((1,0))) + self.rect.center
        self.slines[2] = OG_SAFTEY[2].rotate(-self.dir.angle_to((1,0))) + self.rect.center
        self.slines[3] = OG_SAFTEY[3].rotate(-self.dir.angle_to((1,0))) + self.rect.center
        self.driver = self.rect.center + self.dir*10
        self.eyes = []
        self.vision = []
        for i in range(8):
            self.eyes.append((
                pg.Vector2(self.rect.center),
                self.dir.rotate(i*(360/8))*75 + self.rect.center
            ))
            self.vision.append(pg.Vector2(self.eyes[i][1]-self.eyes[i][0]).magnitude())

    def draw(self, win, fcolor):
        if self.col: color = RED + (fcolor[3],)
        else: color = GREEN + (fcolor[3],)
        for eye,view in zip(self.eyes,self.vision):
            pg.draw.line(win, BLUE, eye[0], eye[1])
            temp = eye[1]-eye[0]
            temp.scale_to_length(view)
            temp += eye[0]
            pg.draw.circle(win, RED, temp, 4)
        if self.id==0:
            fcolor = ORANGE + (fcolor[3],)
        pg.draw.polygon(win, fcolor, self.slines)
        pg.draw.lines(win, color, True, self.slines, 3)
        pg.draw.circle(win, WHITE, self.driver, 2)

class Map:
    def __init__(self, win, maptoload=None):
        self.walls = []
        if maptoload==None:
            self.draw_map(win)
        else:
            self.walls, self.start, self.finish = self.load_map(maptoload)

        temp = (self.finish[0] - self.finish[1])/2
        self.finish_cen = temp + self.finish[0]
    
    def save_map(self, map, start, finish, loc = "map.csv"):
        f = open(loc, "w")
        writer = csv.writer(f)
        writer.writerow(["x1","y1","x2","y2"])
        writer.writerow([start.x, start.y, 0,0])
        writer.writerow([finish[0].x, finish[0].y, finish[1].x, finish[1].y])
        for line in map:
            writer.writerow([line[0].x, line[0].y, line[1].x, line[1].y])
        f.close()

    def draw_map(self, win):
        creating = True
        cmap = []
        print("Map Draw Mode")
        cm = False
        cstart, cend = None, None
        selstart = False
        selfinish = False
        start = None
        finish = None
        cfin = False
        while creating:
            win.fill(DARKGRAY)

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    creating = False
                elif event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        creating = False
                    elif event.key == pg.K_RETURN:
                        if cm:
                            cm = False
                        else:
                            creating = False
                    elif event.key == pg.K_s:
                        selstart = True
                    elif event.key == pg.K_f:
                        selfinish = True
                elif event.type == pg.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if selstart:
                            selstart = False
                            start = pg.Vector2(pg.mouse.get_pos())
                        elif selfinish:
                            if not cfin:
                                cfin = True
                                cstart = pg.Vector2(pg.mouse.get_pos())
                            elif cfin:
                                cfin = False
                                selfinish = False
                                finish = (cstart, pg.Vector2(pg.mouse.get_pos()))
                        elif not cm:
                            cstart = pg.Vector2(pg.mouse.get_pos())
                            cm = True
                        elif cm:
                            cend = pg.Vector2(pg.mouse.get_pos())
                            cmap.append((cstart,cend))
                            cstart = cend

            if cm:
                pg.draw.line(win, (255,255,255),cstart,pg.mouse.get_pos(), 5)

            for p in cmap:
                pg.draw.line(win, GRAY, p[0], p[1])

            if start!=None:
                pg.draw.circle(win, GREEN, start, 5)
            if type(finish)==tuple:
                pg.draw.line(win, RED, finish[0],finish[1], 5)

            pg.display.flip()
        
        self.save_map(cmap, start, finish)
        self.walls = cmap
        self.start = start
        self.finish = finish
        temp = (self.finish[0] - self.finish[1])/2
        self.finish_cen = temp + self.finish[0]

    def load_map(self, loc = "map.csv"):
        f = open(loc)
        reader = csv.DictReader(f)
        cmap = []
        start = None
        finish = None
        for line in reader:
            if start==None:
                start = pg.Vector2(round(float(line["x1"])),round(float(line["y1"])))
            elif finish == None:
                p1 = pg.Vector2(round(float(line["x1"])),round(float(line["y1"])))
                p2 = pg.Vector2(round(float(line["x2"])),round(float(line["y2"])))
                finish = (p1,p2)
            else:
                p1 = pg.Vector2(round(float(line["x1"])),round(float(line["y1"])))
                p2 = pg.Vector2(round(float(line["x2"])),round(float(line["y2"])))
                cmap.append((p1,p2))
        f.close()
        return cmap, start, finish
    
    def blit_map(self, win):
        pg.draw.circle(win, (0,255,0), self.start, 5)
        pg.draw.line(win, (255,0,0), self.finish[0], self.finish[1], 5)
        for p in self.walls:
            pg.draw.line(win, GRAY, p[0], p[1])

class CarGame:
    def __init__(self, WIDTH, HEIGHT, map=None):
        self.clk = pg.time.Clock()

        self.win = pg.display.set_mode((WIDTH, HEIGHT), pg.FULLSCREEN, pg.HWSURFACE)

        self.fps = 20

        self.map = Map(self.win, map)

        self.UP = False
        self.LEFT = False
        self.RIGHT = False
        self.DOWN = False

    def frame(self, pls, tscoreid=0):
        self.win.fill(BLACK)
        self.map.blit_map(self.win)
        gas = 0
        turn = 0
        for event in pg.event.get():
            if event.type == pg.QUIT:
                return False, pls
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    return False, pls
                if event.key == pg.K_RETURN:
                    return True, pls
                elif len(pls)==1:
                    if event.key == pg.K_UP:
                        self.UP = True
                    elif event.key == pg.K_DOWN:
                        self.DOWN = True
                    elif event.key == pg.K_LEFT:
                        self.LEFT = True
                    elif event.key == pg.K_RIGHT:
                        self.RIGHT = True
            elif event.type == pg.KEYUP:
                if len(pls)==1:
                    if event.key == pg.K_UP:
                        self.UP = False
                    elif event.key == pg.K_DOWN:
                        self.DOWN = False
                    elif event.key == pg.K_LEFT:
                        self.LEFT = False
                    elif event.key == pg.K_RIGHT:
                        self.RIGHT = False

        if self.UP:
            gas = 0.5
        elif self.DOWN:
            gas = -0.5
        if self.LEFT:
            turn = -0.5
        elif self.RIGHT:
            turn = 0.5

        for pl in pls:
            pl.update(pl.next_move[0], pl.next_move[1])
            for line in self.map.walls:
                if intersect(pl.slines[0], pl.slines[1], line[0], line[1]) or intersect(pl.slines[2], pl.slines[3], line[0], line[1]):
                    gas = 0
                    pl.col = True
                    break
                elif intersect(pl.slines[1], pl.slines[2], line[0], line[1]) or intersect(pl.slines[3], pl.slines[1], line[0], line[1]):
                    gas = 0
                    pl.col = True
                    break
                elif intersect(pl.slines[0], pl.slines[1], self.map.finish[0], self.map.finish[1]) or intersect(pl.slines[2], pl.slines[3], self.map.finish[0], self.map.finish[1]):
                    pl.col = True
                    pl.goal = True
                else:
                    pl.col = False
                i=0
                for eye in pl.eyes:
                    if intersect(eye[0], eye[1], line[0], line[1]):
                        ip = pg.Vector2(inter_p(eye[0], eye[1], line[0], line[1]))
                        mag = ip-pl.rect.center
                        if mag.magnitude() < pl.vision[i]:
                            pl.vision[i] = mag.magnitude()
                    i+=1
            c = GREEN + (100,)
            if pl.id == tscoreid:
                c = GREEN + (255,)
            pl.draw(self.win, c)

        pg.display.flip()
        self.clk.tick(self.fps)
        return True, pls

class Population:
    def __init__(self, num_players, start, finish, lhero = False):
        self.last_best = Car(0,0, -1)
        self.sec_last_best = Car(0,0, -1)
        self.players = []
        self.start_time = 0
        self.alive = num_players
        self.num_players = num_players
        for i in range(num_players):
            self.players.append(Car(start.x, start.y, i))
        self.start = start
        self.finish = finish
        self.topscore = (0,0)
        self.secscore = (0,0)
        if lhero:
            self.players[0].brain.load_brain()
    
    def update(self, end):
        dead = []
        i = 0
        for p in self.players:
            if p.col:
                self.alive -= 1
                dead.append(i)
            else:
                p.next_move = p.brain.get_pred(np.array(p.vision)/75.0)
                p.score = 1/self.finish.distance_to(p.rect.center)
                if p.score > self.topscore[1]:
                    if p.id!=self.topscore[0]:
                        print(p.id, "in first")
                        print(self.topscore[0], "in second")
                    self.secscore = copy(self.topscore)
                    self.topscore = (p.id, p.score)
                elif p.score > self.secscore[1] and p.id!=self.topscore[0]:
                    if p.id!=self.secscore[0]:
                        print(p.id, "in second")
                    self.secscore = (p.id, p.score)
            i+=1
        dead.reverse()
        for j in dead:
            cp = self.players.pop(j)
            cp.score = 1/self.finish.distance_to(cp.rect.center)
            if cp.goal:
                cp.score += 1/(time_ns() - self.start_time)
            if cp.id==self.topscore[0]:
                self.last_best = deepcopy(cp)
            elif cp.id==self.secscore[0]:
                self.sec_last_best = deepcopy(cp)

    def breed(self, parents):
        for p in self.players:
            p.col = True
        self.update(self.finish)
        print("High score was:", parents[0].score, "by", parents[0].id)
        self.topscore = (0,0)
        self.players = []
        for i in range(self.num_players):
            self.players.append(Car(self.start.x, self.start.y, i))
        self.alive = self.num_players
        self.players[0].brain = deepcopy(parents[0].brain)
        for p in self.players[1:]:
            p.brain = deepcopy(parents[0].brain)
            i=0
            for layer in p.brain.layers:
                if np.random.randint(1,11)>5:
                    layer.weight.data = deepcopy(parents[1].brain.layers[i].weight.data)
                i+=1
            p.brain.mutate()

if __name__=="__main__":
    pg.init()
    g = CarGame(1920,1080, "map.csv")
    pop = Population(40, g.map.start, g.map.finish_cen,lhero=False)
    #pop.players[0].brain = straighten()
    timeout = 25
    ret = True
    save_hero = True
    for j in range(150):
        sleep(2)
        print("Generation:", j+1)
        pop.start_time = time_ns()
        init_t = round(time())
        while pop.alive:
            ret, pop.players = g.frame(pop.players, pop.topscore[0])
            if not ret:
                break
            pop.update(g.map.finish_cen)
            if round(time()) - init_t > timeout:
                break
        if pop.last_best.goal:
            pop.last_best.brain.draw_graph(g.win, 1920, 1080, 8, (0,255,0))
        pop.breed([pop.last_best, pop.sec_last_best])
        #timeout += 2.5
        if not ret:
            break
    if save_hero:
        pop.last_best.brain.save_brain()


    pg.quit()